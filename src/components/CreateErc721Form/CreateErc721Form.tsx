import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import styled from 'styled-components';
import { CustomErc721Token } from 'typings/forms';

import InfoTooltip from 'components/InfoTooltip';
import Input from 'components/Input';

import { max, maxLength, min, minLength, name, number, required, symbol, url } from 'utils/validators';

const CreateErc721FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;

  .create-erc721-token-form__base-info,
  .create-erc721-token-form__description {
    display: grid;
    gap: 12px;

  }

  .create-erc721-token-form__base-info {
    grid-template-columns: 2fr 1fr;
  }

  .create-erc721-token-form__description {
    grid-template-columns: 1fr 2fr;
  }

  .create-erc721-token-form__uri {
    display: flex;
    margin-bottom: 12px;
  }
`;

interface Props {
  disabled?: boolean;
  onChange: (form: Form<CustomErc721Token>) => void;
}

function CreateErc721Form ({
  disabled = false,
  onChange
}: Props) {
  const { t } = useTranslation();

  const form = useForm({
    initialValues: {
      name: '',
      symbol: '',
      contractURI: '',
      baseURI: '',
      totalSupplyCap: 1000,
      burnable: '',
      transferable: '',
    },
    validators: {
      name: [required, name, minLength(3), maxLength(40)],
      symbol: [required, symbol, minLength(1), maxLength(5)],
      contractURI: [url],
      baseURI: [required, url],
      totalSupplyCap: [required, number, min(1), max('10000000000')],
    },
  });

  useEffect(() => {
    onChange(form);
  }, [form.values, onChange]);

  return (
    <CreateErc721FormContainer>
      <div className="create-erc721-token-form__base-info">
        <Input
          {...form.fields.name}
          label={t('TOKEN_NAME')}
          placeholder={t('TOKEN_NAME')}
          disabled={disabled}
        />
        <Input
          {...form.fields.symbol}
          label={t('TOKEN_SYMBOL')}
          placeholder={t('TOKEN_SYMBOL')}
          disabled={disabled}
        />
      </div>
      <div className="create-erc721-token-form__description">
        <Input
          {...form.fields.baseURI}
          label={t('TOKEN_BASE_URI')}
          placeholder={t('TOKEN_BASE_URI')}
          disabled={disabled}
        />
        <Input
          {...form.fields.totalSupplyCap}
          label={t('TOKEN_TOTAL_SUPPLY')}
          placeholder={t('TOKEN_TOTAL_SUPPLY')}
          disabled={disabled}
        />
        <Input
          {...form.fields.transferable}
          label={t('TOKEN_TRANSFERABLE')}
          placeholder={t('TOKEN_TRANFERABLE')}
          disabled={disabled}
        />
        <Input
          {...form.fields.burnable}
          label={t('TOKEN_BURNABLE')}
          placeholder={t('TOKEN_BURNABLE')}
          disabled={disabled}
        />
      </div>
      <div>
        <div className="create-erc721-token-form__uri">
          <p className="text-md">
            {t('TOKEN_CONTRACT_URI')}
          </p>
          <InfoTooltip description={t('TOKEN_CONTRACT_URI_TOOLTIP')} />
        </div>
        <Input
          {...form.fields.contractURI}
          placeholder={t('TOKEN_CONTRACT_URI')}
          disabled={disabled}
        />
      </div>
    </CreateErc721FormContainer>
  );
}

export default CreateErc721Form;
