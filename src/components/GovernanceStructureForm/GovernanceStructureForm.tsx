import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import { Check } from '@q-dev/q-ui-kit';
import styled from 'styled-components';
import { GovernanceStructureFormParameter } from 'typings/forms';

import { required } from 'utils/validators';

const GovernanceStructureFormContainer = styled.div`
  display: grid;
  gap: 16px;
`;

interface Props {
  disabled?: boolean;
  name: string;
  onChange: (form: Form<GovernanceStructureFormParameter>) => void;
}

function GovernanceStructureForm ({ onChange, name }: Props) {
  const { t } = useTranslation();

  const form = useForm({
    initialValues: {
      isGeneraleUpdateVote: true,
      isParameterVote: true,
    },
    validators: {
      isGeneraleUpdateVote: [required],
      isParameterVote: [required],
    },
  });

  useEffect(() => {
    onChange(form);
  }, [form.values, onChange]);

  return (
    <GovernanceStructureFormContainer>
      <Check
        {...form.fields.isGeneraleUpdateVote}
        value={Boolean(form.values.isGeneraleUpdateVote)}
        label={t('EXPERT_GENERAL_UPDATE_VOTE', { name })}
      />
      <Check
        {...form.fields.isParameterVote}
        value={Boolean(form.values.isParameterVote)}
        label={t('EXPERT_PARAMETER_VOTE', { name })}
      />
    </GovernanceStructureFormContainer>
  );
}

export default GovernanceStructureForm;
