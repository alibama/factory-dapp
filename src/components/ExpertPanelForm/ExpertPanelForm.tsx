import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import { Textarea } from '@q-dev/q-ui-kit';
import styled from 'styled-components';
import { ExpertPanelFormParameter } from 'typings/forms';

import Input from 'components/Input';

import { maxLength, minLength, name, required } from 'utils/validators';

const ExpertPanelFormContainer = styled.div`
  display: grid;
  gap: 16px;
`;

interface Props {
  disabled?: boolean;
  onChange: (form: Form<ExpertPanelFormParameter>) => void;
}

function ExpertPanelForm ({
  disabled = false,
  onChange
}: Props) {
  const { t } = useTranslation();

  const form = useForm({
    initialValues: {
      name: '',
      purpose: '',
    },
    validators: {
      name: [required, name, minLength(3), maxLength(30)],
      purpose: [required],
    },
  });

  useEffect(() => {
    onChange(form);
  }, [form.values, onChange]);

  return (
    <ExpertPanelFormContainer>
      <Input
        {...form.fields.name}
        label={t('EXPERT_PANEL_NAME')}
        disabled={disabled}
      />
      <Textarea
        {...form.fields.purpose}
        rows={3}
        maxLength={1000}
        label={t('EXPERT_PANEL_PURPOSE')}
        disabled={disabled}
      />
    </ExpertPanelFormContainer>
  );
}

export default ExpertPanelForm;
