import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import { Icon, Textarea } from '@q-dev/q-ui-kit';
import styled from 'styled-components';
import { DescribePurposeFormParameter } from 'typings/forms';

import Button from 'components/Button';

import { required } from 'utils/validators';

const DescribePurposeFormContainer = styled.div`
  display: flex;
  align-items: flex-start;
  gap: 8px;
`;

interface Props {
  disabled?: boolean;
  count: number;
  length: number;
  onChange: (form: Form<DescribePurposeFormParameter>) => void;
  onRemove: () => void;
}

function DescribePurposeForm ({
  disabled = false,
  count,
  length,
  onChange,
  onRemove
}: Props) {
  const { t } = useTranslation();

  const form = useForm({
    initialValues: {
      description: '',
    },
    validators: {
      description: [required],
    },
  });

  useEffect(() => {
    onChange(form);
  }, [form.values, onChange]);

  return (
    <DescribePurposeFormContainer>
      <Textarea
        {...form.fields.description}
        maxLength={100}
        rows={1}
        label={t('DAO_PURPOSE_LABEL', { index: count > 1 ? count : '' })}
        placeholder={t('DAO_PURPOSE_PLACEHOLDER')}
        disabled={disabled}
      />
      {length > 1 &&
        <Button
          icon
          disabled={form.isSubmitting}
          className="form-block__edit-btn"
          look="ghost"
          onClick={onRemove}
        >
          <Icon name="delete" />
        </Button>
      }
    </DescribePurposeFormContainer>
  );
}

export default DescribePurposeForm;
