import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import styled from 'styled-components';
import { CustomErc20Token } from 'typings/forms';

import InfoTooltip from 'components/InfoTooltip';
import Input from 'components/Input';

import { max, maxLength, min, minLength, name, number, required, symbol, url } from 'utils/validators';

const CreateErc20FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 12px;

  .create-erc20-token-form__base-info,
  .create-erc20-token-form__description {
    display: grid;
    gap: 12px;

  }

  .create-erc20-token-form__base-info {
    grid-template-columns: 2fr 1fr;
  }

  .create-erc20-token-form__description {
    grid-template-columns: 1fr 2fr;
  }

  .create-erc20-token-form__uri {
    display: flex;
    margin-bottom: 12px;
  }
`;

interface Props {
  disabled?: boolean;
  onChange: (form: Form<CustomErc20Token>) => void;
}

function CreateErc20Form ({
  disabled = false,
  onChange
}: Props) {
  const { t } = useTranslation();

  const form = useForm({
    initialValues: {
      name: '',
      decimals: '18',
      symbol: '',
      totalSupplyCap: '1000000000',
      contractURI: '',
    },
    validators: {
      name: [required, name, minLength(3), maxLength(40)],
      decimals: [required, number, min(0), max(27)],
      symbol: [required, symbol, minLength(1), maxLength(5)],
      totalSupplyCap: [required, number, min(1), max('1000000000000000000')],
      contractURI: [url],
    },
  });

  useEffect(() => {
    onChange(form);
  }, [form.values, onChange]);

  return (
    <CreateErc20FormContainer>
      <div className="create-erc20-token-form__base-info">
        <Input
          {...form.fields.name}
          label={t('TOKEN_NAME')}
          placeholder={t('TOKEN_NAME')}
          disabled={disabled}
        />
        <Input
          {...form.fields.symbol}
          label={t('TOKEN_SYMBOL')}
          placeholder={t('TOKEN_SYMBOL')}
          disabled={disabled}
        />
      </div>
      <div className="create-erc20-token-form__description">
        <Input
          {...form.fields.decimals}
          label={t('TOKEN_DECIMALS')}
          placeholder={t('TOKEN_DECIMALS')}
          disabled={disabled}
        />
        <Input
          {...form.fields.totalSupplyCap}
          label={t('TOKEN_TOTAL_SUPPLY')}
          placeholder={t('TOKEN_TOTAL_SUPPLY')}
          disabled={disabled}
        />
      </div>
      <div>
        <div className="create-erc20-token-form__uri">
          <p className="text-md">
            {t('TOKEN_CONTRACT_URI')}
          </p>
          <InfoTooltip description={t('TOKEN_CONTRACT_URI_TOOLTIP')} />
        </div>
        <Input
          {...form.fields.contractURI}
          placeholder={t('TOKEN_CONTRACT_URI')}
          disabled={disabled}
        />
      </div>
    </CreateErc20FormContainer>
  );
}

export default CreateErc20Form;
