
import { URL_REGEX } from '@q-dev/form-hooks';
import { Icon } from '@q-dev/q-ui-kit';

interface Props {
  link: string;
}

function LinkViewer ({ link }: Props) {
  return (
    <div>
      {URL_REGEX.test(link)
        ? (
          <a
            href={link}
            target="_blank"
            rel="noreferrer"
            className="link text-md"
            style={{ maxWidth: '100%' }}
          >
            <span className="ellipsis">{link}</span>
            <Icon name="external-link" />
          </a>
        )
        : <p className="text-md ellipsis">{link || '–'}</p>
      }
    </div>
  );
}

export default LinkViewer;
