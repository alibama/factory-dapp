import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import { FieldOptions, Select, } from '@q-dev/q-ui-kit';
import styled from 'styled-components';
import { CustomExpertPanelVoteFormParameter } from 'typings/forms';

import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

const CustomExpertPanelVoteFormContainer = styled.div`
  display: grid;
  gap: 16px;
`;

interface Props {
  options: FieldOptions<string>;
  index: number;
  defaultValue: string;
  disabled?: boolean;
  onChange: (form: Form<CustomExpertPanelVoteFormParameter>) => void;
}

function CustomExpertPanelVoteForm ({ onChange, options, index, defaultValue }: Props) {
  const { t } = useTranslation();
  const { values } = useDaoCreationForm();

  const form = useForm({
    initialValues: {
      parameterVote: defaultValue,
      generalUpdateVote: defaultValue,
    },
    validators: {
      parameterVote: [],
      generalUpdateVote: [],
    },
  });

  useEffect(() => {
    onChange(form as Form<CustomExpertPanelVoteFormParameter>);
  }, [form.values, onChange]);

  useEffect(() => {
    form.fields.parameterVote.onChange(values.governanceSpecificRoles[index]?.isParameterVote ? defaultValue : 'default-no-veto');
    form.fields.generalUpdateVote.onChange(values.governanceSpecificRoles[index]?.isGeneraleUpdateVote ? defaultValue : 'default-no-veto');
  }, [defaultValue, values.governanceSpecificRoles, index]);

  return (
    <CustomExpertPanelVoteFormContainer>
      <Select
        {...form.fields.parameterVote}
        value={form.values.parameterVote}
        label={t('CONSTITUTION_VOTE')}
        disabled={!values.governanceSpecificRoles[index]?.isParameterVote}
        placeholder={t('SELECT_OPTION')}
        options={options}
      />
      <Select
        {...form.fields.generalUpdateVote}
        value={form.values.generalUpdateVote}
        label={t('GENERAL_UPDATE_VOTE')}
        disabled={!values.governanceSpecificRoles[index]?.isGeneraleUpdateVote}
        placeholder={t('SELECT_OPTION')}
        options={options}
      />
    </CustomExpertPanelVoteFormContainer>
  );
}

export default CustomExpertPanelVoteForm;
