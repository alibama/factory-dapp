import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import { FieldOptions, Select, } from '@q-dev/q-ui-kit';
import styled from 'styled-components';
import { GeneralMembersVoteFormParameter } from 'typings/forms';

import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

const GeneralMembersVoteFormContainer = styled.div`
  display: grid;
  gap: 16px;
`;

interface Props {
  options: FieldOptions<string>;
  index: number;
  defaultValue: string;
  disabled?: boolean;
  onChange: (form: Form<GeneralMembersVoteFormParameter>) => void;
}

function GeneralMembersVoteForm ({ onChange, options, index, defaultValue }: Props) {
  const { t } = useTranslation();
  const { values } = useDaoCreationForm();

  const form = useForm({
    initialValues: {
      membershipVote: defaultValue,
    },
    validators: {
      membershipVote: [],
    },
  });

  useEffect(() => {
    onChange(form as Form<GeneralMembersVoteFormParameter>);
  }, [form.values, onChange]);

  useEffect(() => {
    form.fields.membershipVote.onChange(values.isMembershipsVote[index]?.isMembershipVote ? defaultValue : '');
  }, [defaultValue]);

  return (
    <GeneralMembersVoteFormContainer>
      <Select
        {...form.fields.membershipVote}
        value={form.values.membershipVote}
        label={t('MEMBERSHIP_VOTE', { name: values.expertPanelRoles[index].name })}
        disabled={!values.isMembershipsVote[index]?.isMembershipVote}
        placeholder={t('SELECT_OPTION')}
        options={options}
      />
    </GeneralMembersVoteFormContainer>
  );
}

export default GeneralMembersVoteForm;
