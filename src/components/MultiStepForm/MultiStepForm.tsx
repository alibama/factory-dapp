import { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';

import { MultiStepFormContainer } from './styles';

interface Props {
  stepIndex: number;
  steps: {
    id: string;
    title: string;
    tip?: string;
    children: ReactNode;
  }[];
}

function MultiStepForm ({ stepIndex, steps }: Props) {
  const { t } = useTranslation();

  return (
    <MultiStepFormContainer $step={stepIndex + 1}>
      <div className="multi-step-form__title">
        <span className="multi-step-form__title-text">
          {t('DAO_CREATION')}
        </span>
        <span className="multi-step-form__title-stepper">
          {t('DAO_CREATION_COUNTER', { count: steps.length, currentCount: stepIndex + 1 })}
        </span>
      </div>
      {steps.map((step, i) => (
        <div
          key={step.id}
          className={stepIndex === i ? 'multi-step-form__step block' : ''}
          data-active={stepIndex === i}
          style={{ display: stepIndex === i ? 'block' : 'none' }}
        >
          <h3 className="text-h3">{step.title}</h3>
          {step.tip && <p className="multi-step-form__step-tip text-md">{step.tip}</p>}

          <div className="multi-step-form__step-content">{step.children}</div>
        </div>
      ))}
    </MultiStepFormContainer>
  );
}

export default MultiStepForm;
