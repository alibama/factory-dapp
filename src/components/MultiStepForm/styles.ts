import styled from 'styled-components';

export const MultiStepFormContainer = styled.div<{ $step: number}>`
  display: flex;
  flex-direction: column;
  gap: 16px;
  margin-top: 16px;

  .multi-step-form__title {
    display: flex;
    justify-content: space-between;
    font-size: 14px;
  }

  .multi-step-form__title-text {
    color: ${({ theme }) => theme.colors.textPrimary};
  }

  .multi-step-form__title-stepper {
    color: ${({ theme }) => theme.colors.textActive};
    font-weight: 600;
  }

  .multi-step-form__step-content {
    margin-top: 24px;
  }

  .multi-step-form__step-tip {
    color: ${({ theme }) => theme.colors.textSecondary};
  }
`;

export const FormStepContainer = styled.form`
  .form-step-content {
    display: grid;
    gap: 16px;
    grid-template-columns: minmax(0, 1fr);
  }

  .form-step-actions {
    margin-top: 32px;
    display: flex;
    justify-content: space-between;
  }

  .form-step-action:last-child {
    margin-left: auto;
  }
`;
