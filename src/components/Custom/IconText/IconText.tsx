
import { Icon } from '@q-dev/q-ui-kit';
import styled from 'styled-components';

const IconTextWrapper = styled.div`
    display: flex;
    gap: 5px;
    margin-bottom: 12px;
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
    align-items: center;
`;

interface Props {
  iconName: string;
  text: string;
}

export default function IconText ({
  iconName,
  text,
  ...rest
}: Props) {
  return (
    <IconTextWrapper {...rest}>
      <Icon name={iconName} />
      <span>{text}</span>
    </IconTextWrapper>
  );
}
