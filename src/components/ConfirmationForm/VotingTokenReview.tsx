
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import LinkViewer from 'components/LinkViewer';
import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

const StyledVotingTokenReview = styled.div`
  display: flex;
  flex-direction: column;

  .voting-token-review__info { 
    display: flex;
    flex-direction: column;
    gap: 4px;
  }

  .voting-token-review__parameters { 
    display: flex;
    flex-direction: column;
    gap: 8px;
  }

  .voting-token-review__info-title {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.textSecondary}; 
  }

  .voting-token-review__title {
    font-size: 14px;
    font-weight: 600;
    margin-bottom: 8px;
    color: ${({ theme }) => theme.colors.textActive};  
  }

  .voting-token-review__q-token,
  .voting-token-review__info-text {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
  }
`;

export default function VotingTokenReview () {
  const { values } = useDaoCreationForm();
  const { t } = useTranslation();

  const customTokenOptions = useMemo(
    () => {
      if (values.tokenType === 'native' || values.tokenType === 'existing-token') return [];
      const customToken = values.customErc20Token || values.customErc721Token;
      return customToken
        ? [
          {
            value: customToken.name,
            label: t('TOKEN_NAME')
          },
          {
            value: customToken.symbol,
            label: t('TOKEN_SYMBOL')
          },
          ...'decimals' in customToken
            ? [{
              value: customToken.decimals,
              label: t('TOKEN_DECIMALS')
            }]
            : [],
          ...'baseURI' in customToken
            ? [{
              value: customToken.baseURI,
              label: t('TOKEN_BASE_URI'),
              isLink: true
            }]
            : [],
          {
            value: customToken.totalSupplyCap,
            label: t('TOKEN_TOTAL_SUPPLY')
          },
          ...customToken.contractURI
            ? [{
              value: customToken.contractURI,
              label: t('TOKEN_CONTRACT_URI'),
              isLink: true
            }]
            : [],
        ]
        : [];
    }, [t, values.customErc20Token, values.tokenType, values.customErc721Token]);

  return (
    <StyledVotingTokenReview>
      {values.tokenType !== 'native'
        ? <>
          <h4 className="voting-token-review__title">
            {t('TOKEN_HOLDERS_OPTION')}
          </h4>
          {(values.tokenType === 'erc20' || values.tokenType === 'erc721') &&
            <div className="voting-token-review__parameters">
              {customTokenOptions.map((item, i) => (
                <div key={i} className="voting-token-review__info">
                  <p className="voting-token-review__info-title">
                    {item.label}
                  </p>
                  {
                    item.isLink
                      ? <LinkViewer link={item.value} />
                      : <p className="voting-token-review__info-text">
                        {item.value}
                      </p>
                  }
                </div>
              ))}
            </div>
          }
          {values.tokenType === 'existing-token' &&
            <div className="voting-token-review__info">
              <p className="voting-token-review__info-title">
                {t('TOKEN_ADDRESS')}
              </p>
              <p className="voting-token-review__info-text">
                {values.tokenAddress}
              </p>
            </div>
          }
        </>
        : <h4 className="voting-token-review__q-token">
          {t('Q_TOKEN_HOLDERS')}
        </h4>
      }
    </StyledVotingTokenReview>
  );
}
