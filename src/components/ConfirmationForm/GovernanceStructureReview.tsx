
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import IconText from 'components/Custom/IconText';
import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

const StyledGovernanceStructureReview = styled.div`
  display: flex;
  flex-direction: column;

  .governance-structure-review__general {
    margin-bottom: 16px;
  }

  .governance-structure-review__title {
    font-size: 14px;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.textActive}; 
    margin-bottom: 8px;
  }

  .governance-structure-review__membership {
    display: flex;
    flex-direction: column;
    border-top: 1px solid ${({ theme }) => theme.colors.borderSecondary}; 
    padding: 16px 0px 0px;
    gap: 8px;
    margin-bottom: 16px;
  }
`;

export default function GovernanceStructureReview () {
  const { values } = useDaoCreationForm();
  const { t } = useTranslation();

  return (
    <StyledGovernanceStructureReview>
      <div className="governance-structure-review__general">
        <h4 className="governance-structure-review__title">
          {t('GENERAL_DAO_MEMBERS')}
        </h4>
        <IconText
          text={t('CONSTITUTION_VOTE')}
          iconName={values.isSetConstitutionVote ? 'check' : 'cross'}
        />
        <IconText
          text={t('GENERAL_UPDATE_VOTE')}
          iconName={values.isSetGeneralUpdateVote ? 'check' : 'cross'}
        />
        {values.isMembershipsVote.map((item, i) => (
          <IconText
            key={i}
            text={t('EXPERT_GENERAL_UPDATE_VOTE', { name: values.expertPanelRoles[i].name })}
            iconName={item.isMembershipVote ? 'check' : 'cross'}
          />
        ))}
      </div>
      {values.governanceSpecificRoles.map((item, i) => (
        <div key={i} className="governance-structure-review__membership">
          <h4 className="governance-structure-review__title">
            {values.expertPanelRoles[i].name}
          </h4>
          <div>
            <IconText
              text={t('GENERAL_UPDATE_VOTE')}
              iconName={item.isGeneraleUpdateVote ? 'check' : 'cross'}
            />
            <IconText
              text={t('PARAMETER_VOTE')}
              iconName={item.isParameterVote ? 'check' : 'cross'}
            />
          </div>
        </div>
      ))}
    </StyledGovernanceStructureReview>
  );
}
