
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';
import { ExpertPanelFormParameter } from 'typings/forms';

const StyledExpertPanelRoleReview = styled.div`
  display: flex;
  flex-direction: column;

  .expert-panel-role-review__title {
    font-size: 14px;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.textActive}; 
    margin-bottom: 8px;
  }

  .expert-panel-role-review__info { 
    display: flex;
    flex-direction: column;
    margin-bottom: 12px;
  }

  .expert-panel-role-review__info-title {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.textSecondary}; 
  }

  .expert-panel-role-review__info-text {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
  }
`;

interface Props {
  count: number;
  expertPanelRole: ExpertPanelFormParameter;
}

export default function ExpertPanelRoleReview ({ expertPanelRole, count }: Props) {
  const { t } = useTranslation();

  return (
    <StyledExpertPanelRoleReview>
      <h4 className="expert-panel-role-review__title">
        {t('EXPERT_PANEL_INDEX', { index: count })}
      </h4>
      <div className="expert-panel-role-review__info">
        <div className="expert-panel-role-review__info-title">
          {t('EXPERT_PANEL_NAME')}
        </div>
        <div className="expert-panel-role-review__info-text">
          {expertPanelRole.name}
        </div>
      </div>
      <div className="expert-panel-role-review__info">
        <div className="expert-panel-role-review__info-title">
          {t('EXPERT_PANEL_PURPOSE')}
        </div>
        <div className="expert-panel-role-review__info-text">
          {expertPanelRole.purpose}
        </div>
      </div>
    </StyledExpertPanelRoleReview>
  );
}
