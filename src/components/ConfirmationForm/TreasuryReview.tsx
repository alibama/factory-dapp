
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

const StyledTreasuryReview = styled.div`
  display: flex;
  flex-direction: column;

  .treasury-review__address { 
    display: flex;
    flex-direction: column;
  }

  .treasury-review__address-title {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.textSecondary}; 
  }

  .treasury-review__title,
  .treasury-review__address-text {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
  }
`;

export default function TreasuryReview () {
  const { values } = useDaoCreationForm();
  const { t } = useTranslation();
  return (
    <StyledTreasuryReview >
      {values.isWithTreasury
        ? <div className="treasury-review__address">
          <div className="treasury-review__address-title">
            {t('TOKEN_ADDRESS')}
          </div>
          <div className="treasury-review__address-text">
            {values.treasuryAddress}
          </div>
        </div>
        : <h4 className="treasury-review__title">
          {t('WITHOUT_TREASURY')}
        </h4>
      }
    </StyledTreasuryReview>
  );
}
