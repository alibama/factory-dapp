
import styled from 'styled-components';

const StyledDescribePurposeReview = styled.div`
  display: flex;
  flex-direction: column;
  
  .describe-purpose-review__title {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.textSecondary}; 
  }

  .describe-purpose-review__text {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
  }
`;

interface Props {
  name: string;
  description: string;
}

export default function DescribePurposeReview ({ name, description }: Props) {
  return (
    <StyledDescribePurposeReview>
      <div className="describe-purpose-review__title">{name}</div>
      <div className="describe-purpose-review__text">{description}</div>
    </StyledDescribePurposeReview>
  );
}
