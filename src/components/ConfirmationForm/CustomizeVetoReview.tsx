
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

const StyledCustomizeVetoReview = styled.div`
  display: flex;
  flex-direction: column;

  .customize-veto-review__general {
    margin-bottom: 16px;
  }

  .customize-veto-review__title {
    font-size: 14px;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.textActive}; 
    margin-bottom: 8px;
  }

  .customize-veto-review__info { 
    display: flex;
    flex-direction: column;
    gap: 5px;
    margin-bottom: 12px;
  }

  .customize-veto-review__info-title {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.textSecondary}; 
  }

  .customize-veto-review__info-text {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
  }

  .customize-veto-review__membership {
    display: flex;
    flex-direction: column;
    border-top: 1px solid ${({ theme }) => theme.colors.borderSecondary}; 
    padding: 16px 0px 0px;
    gap: 8px;
    margin-bottom: 16px;
  }
`;

export default function CustomizeVetoReview () {
  const { values } = useDaoCreationForm();
  const { t } = useTranslation();

  const vetoOptions = useMemo(
    () => {
      return [
        {
          value: 'default-no-veto',
          label: t('NO_VETO')
        },
        ...values.guardianExpertPanel
          ? [{
            value: values.guardianExpertPanel.name,
            label: values.guardianExpertPanel.name
          }]
          : [],
        ...values.expertPanelRoles.map(item => ({ value: item.name, label: item.name }))
      ];
    },
    [values.expertPanelRoles, values.guardianExpertPanel]);

  const findVetoOption = (searchValue: string) => {
    const vetoOption = vetoOptions.find(item => item.value === searchValue);
    return vetoOption;
  };

  const findVetoOptionName = (searchValue: string) => {
    const vetoOption = findVetoOption(searchValue);
    return vetoOption?.label || t('NO_VETO');
  };

  return (
    <StyledCustomizeVetoReview >
      <div className="customize-veto-review__general">
        <h4 className="customize-veto-review__title">
          {t('GENERAL_DAO_MEMBERS')}
        </h4>
        <div className="customize-veto-review__info">
          <div className="customize-veto-review__info-title">
            {t('CONSTITUTION_VOTE')}
          </div>
          <div className="customize-veto-review__info-text">
            {findVetoOptionName(values.constitutionVote)}
          </div>
        </div>
        <div className="customize-veto-review__info">
          <div className="customize-veto-review__info-title">
            {t('GENERAL_UPDATE_VOTE')}
          </div>
          <div className="customize-veto-review__info-text">
            {findVetoOptionName(values.generalUpdateVote)}
          </div>
        </div>

        {values.membershipVotes.map((item, i) => (
          <div key={i} className="customize-veto-review__info">
            <div className="customize-veto-review__info-title">
              {t('MEMBERSHIP_VOTE', { name: values.expertPanelRoles[i].name })}
            </div>
            <div className="customize-veto-review__info-text">
              {findVetoOptionName(item.membershipVote)}
            </div>
          </div>
        ))}
      </div>
      {values.expertPanelVote.map((item, i) => (
        <div key={i} className="customize-veto-review__membership">
          <h4 className="customize-veto-review__title">
            {values.expertPanelRoles[i].name}
          </h4>
          <div>
            <div className="customize-veto-review__info">
              <div className="customize-veto-review__info-title">
                {t('GENERAL_UPDATE_VOTE')}
              </div>
              <div className="customize-veto-review__info-text">
                {findVetoOptionName(item.generalUpdateVote)}
              </div>
            </div>
            <div className="customize-veto-review__info">
              <div className="customize-veto-review__info-title">
                {t('CONSTITUTION_VOTE')}
              </div>
              <div className="customize-veto-review__info-text">
                {findVetoOptionName(item.parameterVote)}
              </div>
            </div>
          </div>
        </div>
      ))}
    </StyledCustomizeVetoReview>
  );
}
