
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import { useDaoCreationForm } from 'pages/DaoCreation/DaoCreation';

import ExpertPanelRoleReview from './ExpertPanelRoleReview';

const StyledVetoPowersReview = styled.div`
  display: flex;
  flex-direction: column;

  .veto-powers__title {
    font-size: 16px;
    color: ${({ theme }) => theme.colors.textPrimary}; 
  }
`;

export default function VetoPowersReview () {
  const { values } = useDaoCreationForm();
  const { t } = useTranslation();

  return (
    <StyledVetoPowersReview>
      {values.vetoPowerType !== 'nothing' &&
        values.guardianExpertPanel
        ? <ExpertPanelRoleReview
          expertPanelRole={values.guardianExpertPanel}
          count={1}
        />
        : <h4 className="veto-powers__title">
          {t('ROLE_WITHOUT_VETO_POWERS')}
        </h4>
      }
    </StyledVetoPowersReview>
  );
}
