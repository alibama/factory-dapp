import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { Form, useForm } from '@q-dev/form-hooks';
import { Check } from '@q-dev/q-ui-kit';
import styled from 'styled-components';
import { MemberVoteFormParameter } from 'typings/forms';

import { required } from 'utils/validators';

const MemberVoteFormContainer = styled.div`
  display: grid;
  gap: 16px;
`;

interface Props {
  disabled?: boolean;
  name: string;
  onChange: (form: Form<MemberVoteFormParameter>) => void;
}

function MemberVoteForm ({ name, onChange }: Props) {
  const { t } = useTranslation();

  const form = useForm({
    initialValues: {
      isMembershipVote: true,
    },
    validators: {
      isMembershipVote: [required],
    },
  });

  useEffect(() => {
    onChange(form);
  }, [form.values, onChange]);

  return (
    <MemberVoteFormContainer>
      <Check
        {...form.fields.isMembershipVote}
        value={Boolean(form.values.isMembershipVote)}
        label={t('MEMBERSHIP_VOTE', { name })}
      />
    </MemberVoteFormContainer>
  );
}

export default MemberVoteForm;
