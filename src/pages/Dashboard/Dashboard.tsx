import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Button, media } from '@q-dev/q-ui-kit';
import styled from 'styled-components';

import logo from 'assets/img/people.svg';
import PageLayout from 'components/PageLayout';

const StyledDashboard = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 48px;
  gap: 8px;

  .dashboard__info {
    display: flex;
    flex-direction: column;
  }

  .dashboard__info-title { 
    color: ${({ theme }) => theme.colors.textPrimary};
    font-size: 32px;
    margin-bottom: 20px;
    line-height: 1.4;
    font-weight: 500;
  }

  .dashboard__info-text { 
    color: ${({ theme }) => theme.colors.textSecondary};
    font-size: 16px;
    margin-bottom: 20px;
  }

  .dashboard__img {
    ${media.lessThan('medium')} {
      display: none;    
    }
  }
`;

export default function Dashboard () {
  const { t } = useTranslation();

  return (
    <PageLayout>
      <StyledDashboard className="block">
        <div className="dashboard__info">
          <h2 className="dashboard__info-title">{t('DAO_CREATION')}</h2>
          <p className="dashboard__info-text">{t('DAO_CREATION_INFO')}</p>
          <div className="dashboard__info-actions">
            <Link to="/dao">
              <Button>{t('CREATE_DAO')}</Button>
            </Link>
          </div>
        </div>
        <img
          className="dashboard__img"
          alt="People icon"
          src={logo}
        />
      </StyledDashboard>
    </PageLayout>
  );
}
