import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { useForm } from '@q-dev/form-hooks';
import { Check } from '@q-dev/q-ui-kit';
import { DaoCreationForm, TreasuryFormParameter } from 'typings/forms';

import Input from 'components/Input';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

import { address, required, requiredIf } from 'utils/validators';

function TreasuryStep () {
  const { t } = useTranslation();
  const { goNext, goBack } = useDaoCreationForm();

  const form = useForm({
    initialValues: {
      isWithTreasury: false,
      treasuryAddress: '',
    },
    validators: {
      isWithTreasury: [required],
      treasuryAddress: [
        requiredIf((_, form) => (form as TreasuryFormParameter).isWithTreasury),
        address,
      ],
    },
    onSubmit: (form) => goNext(form as DaoCreationForm),
  });

  useEffect(() => {
    if (!form.values.isWithTreasury) {
      form.values.treasuryAddress = '';
      form.validate();
    }
  }, [form.fields.isWithTreasury.value]);

  return (
    <FormStep
      disabled={!form.isValid}
      onNext={form.submit}
      onBack={goBack}
    >
      <Check
        {...form.fields.isWithTreasury}
        value={Boolean(form.values.isWithTreasury)}
        label={t('TREASURE_CHECK_LABEL')}
      />
      {form.values.isWithTreasury &&
        <Input
          {...form.fields.treasuryAddress}
          label={t('ADDRESS')}
          placeholder={t('TREASURE_INPUT_PLACEHOLDER')}
        />}
    </FormStep>
  );
}

export default TreasuryStep;
