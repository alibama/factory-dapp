import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { address, requiredIf, useForm, useFormArray } from '@q-dev/form-hooks';
import { RadioGroup, RadioOptions } from '@q-dev/q-ui-kit';
import { CustomErc20Token, CustomErc721Token, DaoCreationForm, VotingTokenFormParameter, VotingTokenType } from 'typings/forms';

import CreateErc20Form from 'components/CreateErc20Form';
import CreateErc721Form from 'components/CreateErc721Form';
import Input from 'components/Input';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

import { required } from 'utils/validators';

function VotingTokenStep () {
  const { t } = useTranslation();
  const { goNext, goBack, onChange } = useDaoCreationForm();

  const mainForm = useForm({
    initialValues: {
      tokenType: 'native' as VotingTokenType,
      tokenAddress: ''
    },
    validators: {
      tokenType: [required],
      tokenAddress: [
        requiredIf((_, form) => (form as VotingTokenFormParameter).tokenType === 'existing-token'),
        address
      ],
    },
    onSubmit: (form) => {
      goNext(form as DaoCreationForm);
    },
  });

  const isErc20Token = mainForm.values.tokenType === 'erc20';
  const isErc721Token = mainForm.values.tokenType === 'erc721';
  const isExistingToken = mainForm.values.tokenType === 'existing-token';

  const formErc20 = useFormArray<CustomErc20Token>({
    maxCount: 1,
    onSubmit: (forms) => {
      onChange({ customErc20Token: forms[0] });
    },
  });

  const formErc721 = useFormArray<CustomErc721Token>({
    maxCount: 1,
    onSubmit: (forms) => {
      onChange({ customErc721Token: forms[0] });
    },
  });

  const panelTypeOptions: RadioOptions<VotingTokenType> = [
    {
      value: 'native',
      label: t('Q_TOKEN_HOLDERS')
    },
    {
      value: 'existing-token',
      label: t('CUSTOM_TOKEN_HOLDERS_OPTION')
    },
    {
      value: 'erc20',
      label: t('NEW_ERC20_TOKEN_HOLDERS_OPTION')
    },
    {
      value: 'erc721',
      label: t('NEW_ERC721_TOKEN_HOLDERS_OPTION'),
    },
  ];

  useEffect(
    () => {
      formErc20.reset(isErc20Token ? 1 : 0);
    }, [isErc20Token]);

  useEffect(
    () => {
      formErc721.reset(isErc721Token ? 1 : 0);
    }, [isErc721Token]);

  const handleSubmit = () => {
    if (!isExistingToken) {
      mainForm.fields.tokenAddress.onChange('');
    }
    if (!mainForm.validate() || !formErc20.validate() || !formErc721.validate()) return;
    formErc721.submit();
    formErc20.submit();
    mainForm.submit();
  };

  return (
    <FormStep
      disabled={!mainForm.isValid || !formErc20.isValid || !formErc721.isValid}
      onNext={handleSubmit}
      onBack={goBack}
    >
      <RadioGroup
        {...mainForm.fields.tokenType}
        name="param-panel-type"
        options={panelTypeOptions}
      />
      {isExistingToken &&
        <Input
          {...mainForm.fields.tokenAddress}
          label={t('TOKEN_ADDRESS')}
          placeholder={t('TOKEN_HOLDERS_INPUT_PLACEHOLDER')}
        />}
      {isErc20Token &&
        formErc20.forms.map((formItem) => (
          <CreateErc20Form
            key={formItem.id}
            onChange={formItem.onChange}
          />
        ))}
      {isErc721Token &&
        formErc721.forms.map((formItem) => (
          <CreateErc721Form
            key={formItem.id}
            onChange={formItem.onChange}
          />
        ))}
    </FormStep>
  );
}

export default VotingTokenStep;
