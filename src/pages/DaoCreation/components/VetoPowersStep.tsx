import { useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { useForm, useFormArray } from '@q-dev/form-hooks';
import { RadioGroup, RadioOptions, Select } from '@q-dev/q-ui-kit';
import { ExpertPanelFormParameter, VetoPowersFormParameter, VetoPowerType } from 'typings/forms';

import ExpertPanelForm from 'components/ExpertPanelForm';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

import { required, requiredIf } from 'utils/validators';

function VetoPowersStep () {
  const { t } = useTranslation();
  const { goNext, goBack, onChange, values } = useDaoCreationForm();

  const form = useForm({
    initialValues: {
      vetoPowerType: 'nothing' as VetoPowerType,
      expertPanelIndex: '',
    },
    validators: {
      vetoPowerType: [required],
      expertPanelIndex: [requiredIf((_, form) => (form as VetoPowersFormParameter).vetoPowerType === 'yes')],
    },
    onSubmit: (form) => {
      onChange({
        vetoPowerType: form.vetoPowerType as VetoPowerType,
        defaultVetoExpertPanel: form.vetoPowerType !== 'nothing'
          ? form.vetoPowerType === 'new-role'
            ? formArray.forms[0].values
            : values.expertPanelRoles[Number(form.expertPanelIndex)]
          : null
      });
      goNext();
    },
  });

  const formArray = useFormArray<ExpertPanelFormParameter>({
    maxCount: 1,
    onSubmit: (forms) => {
      onChange({ guardianExpertPanel: forms[0] });
    },
  });

  const panelTypeOptions: RadioOptions<VetoPowerType> = [
    {
      value: 'yes',
      label: t('VETO_POWERS_RADIO_YES'),
      disabled: !values.expertPanelRoles.length
    },
    {
      value: 'new-role',
      label: t('VETO_POWERS_RADIO_NEW_ROLE')
    },
    {
      value: 'nothing',
      label: t('VETO_POWERS_RADIO_NO')
    },
  ];

  const selectOptions = useMemo(() => {
    return values.expertPanelRoles.map((item, index) => ({ value: String(index), label: item.name }));
  }, [values.expertPanelRoles]);

  useEffect(
    () => {
      formArray.reset(form.values.vetoPowerType === 'new-role' ? 1 : 0);
    }, [form.values.vetoPowerType]);

  const handleSubmit = () => {
    if (!form.validate() || !formArray.validate()) return;
    formArray.submit();
    form.submit();
  };

  return (
    <FormStep
      disabled={!form.isValid || !formArray.isValid}
      onNext={handleSubmit}
      onBack={goBack}
    >
      <RadioGroup
        {...form.fields.vetoPowerType}
        label={t('VETO_POWERS_RADIO_LABEL')}
        name="param-panel-type"
        options={panelTypeOptions}
      />
      {form.values.vetoPowerType === 'yes' &&
        <Select
          {...form.fields.expertPanelIndex}
          label={t('VETO_POWERS_SELECT_LABEL')}
          placeholder={t('VETO_POWERS_SELECT_LABEL')}
          options={selectOptions}
        />
      }
      {form.values.vetoPowerType === 'new-role' &&
        formArray.forms.map((formItem) => (
          <ExpertPanelForm
            key={formItem.id}
            onChange={formItem.onChange}
          />
        ))
      }
    </FormStep>
  );
}

export default VetoPowersStep;
