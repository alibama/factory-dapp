import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { useForm, useFormArray } from '@q-dev/form-hooks';
import { Check } from '@q-dev/q-ui-kit';
import { GovernanceStructureFormParameter, MemberVoteFormParameter } from 'typings/forms';

import FormBlock from 'components/FormBlock';
import GovernanceStructureForm from 'components/GovernanceStructureForm';
import MemberVoteForm from 'components/MemberVoteForm';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

import { required } from 'utils/validators';

function GovernanceStructureStep () {
  const { t } = useTranslation();
  const { goNext, goBack, onChange, values } = useDaoCreationForm();

  const form = useForm({
    initialValues: {
      isSetConstitutionVote: true,
      isSetGeneralUpdateVote: true
    },
    validators: {
      isSetConstitutionVote: [required],
      isSetGeneralUpdateVote: [required]
    },
    onSubmit: (form) => goNext(form),
  });

  const formArray = useFormArray<GovernanceStructureFormParameter>({
    onSubmit: (forms) => {
      onChange({ governanceSpecificRoles: forms });
    },
  });

  const formMembersArray = useFormArray<MemberVoteFormParameter>({
    onSubmit: (forms) => {
      onChange({ isMembershipsVote: forms });
    },
  });

  const handleSubmit = () => {
    if (
      !formArray.validate() ||
      !formMembersArray.validate() ||
      !form.validate()
    ) return;
    formArray.submit();
    formMembersArray.submit();
    form.submit();
  };

  useEffect(() => {
    formArray.reset(values.expertPanelRoles.length);
    formMembersArray.reset(values.expertPanelRoles.length);
  }, [values.expertPanelRoles]);

  return (
    <FormStep
      disabled={!form.isValid || !formArray.isValid || !formMembersArray.isValid}
      onNext={handleSubmit}
      onBack={goBack}
    >
      <FormBlock
        title={t('GENERAL_DAO_MEMBERS')}
      >
        <Check
          {...form.fields.isSetConstitutionVote}
          disabled
          value={Boolean(form.values.isSetConstitutionVote)}
          label={t('CONSTITUTION_VOTE')}
        />
        <Check
          {...form.fields.isSetGeneralUpdateVote}
          disabled
          value={Boolean(form.values.isSetGeneralUpdateVote)}
          label={t('GENERAL_UPDATE_VOTE')}
        />
        {formMembersArray.forms.map((formItem, i) => (
          <MemberVoteForm
            key={formItem.id}
            name={values.expertPanelRoles[i]?.name}
            onChange={formItem.onChange}
          />
        ))}
      </FormBlock>

      {formArray.forms.map((formItem, i) => (
        <FormBlock
          key={formItem.id}
          title={values.expertPanelRoles[i]?.name}
        >
          <GovernanceStructureForm
            name={values.expertPanelRoles[i]?.name}
            onChange={formItem.onChange}
          />
        </FormBlock>
      ))}
    </FormStep>
  );
}

export default GovernanceStructureStep;
