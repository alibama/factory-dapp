import { useTranslation } from 'react-i18next';

import { useFormArray } from '@q-dev/form-hooks';
import { Icon } from '@q-dev/q-ui-kit';
import { ExpertPanelFormParameter } from 'typings/forms';

import Button from 'components/Button';
import ExpertPanelForm from 'components/ExpertPanelForm';
import FormBlock from 'components/FormBlock';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

function ExpertPanelStep () {
  const { t } = useTranslation();
  const { goNext, goBack, onChange } = useDaoCreationForm();
  const MAX_PANELS_COUNT = 10;

  const formArray = useFormArray<ExpertPanelFormParameter>({
    maxCount: MAX_PANELS_COUNT,
    onSubmit: (forms) => {
      onChange({ expertPanelRoles: forms });
    },
  });

  const handleSubmit = () => {
    if (!formArray.validate()) return;
    formArray.submit();
    goNext();
  };

  return (
    <FormStep
      disabled={!formArray.isValid}
      submitBtnText={formArray.forms.length ? t('NEXT') : t('SKIP')}
      onNext={handleSubmit}
      onBack={goBack}
    >
      {formArray.forms.map((formItem, i) => (
        <FormBlock
          key={formItem.id}
          title={t('EXPERT_PANEL_INDEX', { index: i + 1 })}
          icon={formArray.forms.length ? 'delete' : undefined}
          onAction={() => formArray.removeForm(formItem.id)}
        >
          <ExpertPanelForm
            onChange={formItem.onChange}
          />
        </FormBlock>
      ))}

      {formArray.forms.length < MAX_PANELS_COUNT &&
        <Button
          look="ghost"
          onClick={formArray.appendForm}
        >
          <Icon name="add" />
          <span>{t('ADD_EXPERT_PANEL')}</span>
        </Button>}
    </FormStep>
  );
}

export default ExpertPanelStep;
