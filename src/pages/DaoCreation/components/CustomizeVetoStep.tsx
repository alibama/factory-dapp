import { useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { useForm, useFormArray } from '@q-dev/form-hooks';
import { Select } from '@q-dev/q-ui-kit';
import { CustomExpertPanelVoteFormParameter, GeneralMembersVoteFormParameter } from 'typings/forms';

import CustomExpertPanelVoteForm from 'components/CustomExpertPanelVoteForm';
import FormBlock from 'components/FormBlock';
import GeneralMembersVoteForm from 'components/GeneralMembersVoteForm';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

function CustomizeVetoStep () {
  const { t } = useTranslation();
  const { goNext, goBack, onChange, values } = useDaoCreationForm();

  const form = useForm({
    initialValues: {
      constitutionVote: '',
      generalUpdateVote: ''
    },
    validators: {
      constitutionVote: [],
      generalUpdateVote: [],
    },
    onSubmit: (form) => {
      goNext(form);
    },
  });

  const formArray = useFormArray<CustomExpertPanelVoteFormParameter>({
    onSubmit: (forms) => {
      onChange({ expertPanelVote: forms });
    },
  });

  const formMembersArray = useFormArray<GeneralMembersVoteFormParameter>({
    onSubmit: (forms) => {
      onChange({ membershipVotes: forms });
    },
  });

  const selectOptions = useMemo(
    () => {
      return [
        {
          value: 'default-no-veto',
          label: t('NO_VETO')
        },
        ...values.guardianExpertPanel
          ? [{
            value: values.guardianExpertPanel.name,
            label: values.guardianExpertPanel.name
          }]
          : [],
        ...values.expertPanelRoles?.map(item => ({ value: item.name, label: item.name }))
      ];
    },
    [values.expertPanelRoles, values.guardianExpertPanel]);

  useEffect(
    () => {
      formArray.reset(values.expertPanelRoles.length);
      formMembersArray.reset(values.expertPanelRoles.length);
    },
    [values.expertPanelRoles]);

  const defaultVetoValue = useMemo(
    () => {
      return values.vetoPowerType === 'new-role' && values.guardianExpertPanel
        ? values.guardianExpertPanel.name
        : values.defaultVetoExpertPanel?.name || selectOptions[0].value;
    },
    [values.defaultVetoExpertPanel]);

  useEffect(
    () => {
      form.fields.constitutionVote.onChange(defaultVetoValue);
      form.fields.generalUpdateVote.onChange(defaultVetoValue);
    },
    [defaultVetoValue]);

  const handleSubmit = () => {
    if (
      !formArray.validate() ||
      !formMembersArray.validate() ||
      !form.validate()
    ) return;
    formArray.submit();
    formMembersArray.submit();
    form.submit();
  };

  return (
    <FormStep
      disabled={!form.isValid || !formArray.isValid || !formMembersArray.isValid}
      onNext={handleSubmit}
      onBack={goBack}
    >
      <FormBlock
        title={t('GENERAL_DAO_MEMBERS')}
      >
        <Select
          {...form.fields.constitutionVote}
          label={t('CONSTITUTION_VOTE')}
          placeholder={t('SELECT_OPTION')}
          options={selectOptions}
        />
        <Select
          {...form.fields.generalUpdateVote}
          label={t('GENERAL_UPDATE_VOTE')}
          placeholder={t('SELECT_OPTION')}
          options={selectOptions}
        />
        {formMembersArray.forms.map((formItem, i) => (
          <GeneralMembersVoteForm
            key={formItem.id}
            index={i}
            defaultValue={defaultVetoValue}
            options={selectOptions}
            onChange={formItem.onChange}
          />
        ))}
      </FormBlock>

      {formArray.forms.map((formItem, i) => (
        <FormBlock
          key={formItem.id}
          title={values.expertPanelRoles[i]?.name}
        >
          <CustomExpertPanelVoteForm
            options={selectOptions}
            index={i}
            defaultValue={defaultVetoValue}
            onChange={formItem.onChange}
          />
        </FormBlock>
      ))}
    </FormStep>
  );
}

export default CustomizeVetoStep;
