
import { useTranslation } from 'react-i18next';

import {
  CustomizeVetoReview,
  DescribePurposeReview,
  ExpertPanelRoleReview,
  GovernanceStructureReview,
  TreasuryReview,
  VetoPowersReview,
  VotingTokenReview
} from 'components/ConfirmationForm';
import FormBlock from 'components/FormBlock';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

function ConfirmationStep () {
  const { goBack, confirm, updateStep, values } = useDaoCreationForm();
  const { t } = useTranslation();

  return (
    <FormStep
      onConfirm={confirm}
      onBack={goBack}
    >
      <FormBlock
        icon="edit"
        title={t('DAO_PURPOSE')}
        onAction={() => updateStep(0)}
      >
        {values.describePurposes.map((item, i) => (
          <DescribePurposeReview
            key={i}
            name={t('PURPOSE_INDEX', { index: i + 1 })}
            description={item.description}
          />
        ))}
      </FormBlock>

      <FormBlock
        icon="edit"
        title={t('DAO_VOTING')}
        onAction={() => updateStep(1)}
      >
        <VotingTokenReview />
      </FormBlock>

      {Boolean(values.expertPanelRoles.length) &&
        (<FormBlock
          icon="edit"
          title={t('DAO_ROLES')}
          onAction={() => updateStep(2)}
        >
          {values.expertPanelRoles.map((item, i) => (
            <ExpertPanelRoleReview
              key={i}
              expertPanelRole={item}
              count={i + 1}
            />
          ))}
        </FormBlock>)
      }

      <FormBlock
        icon="edit"
        title={t('GOVERNANCE_STRUCTURE')}
        onAction={() => updateStep(3)}
      >
        <GovernanceStructureReview />
      </FormBlock>

      <FormBlock
        icon="edit"
        title={t('DAO_TREASURY')}
        onAction={() => updateStep(4)}
      >
        <TreasuryReview />
      </FormBlock>

      <FormBlock
        icon="edit"
        title={t('CHECKS_AND_BALANCES')}
        onAction={() => updateStep(5)}
      >
        <VetoPowersReview />
      </FormBlock>

      <FormBlock
        icon="edit"
        title={t('CUSTOMIZED_VETO')}
        onAction={() => updateStep(6)}
      >
        <CustomizeVetoReview />
      </FormBlock>
    </FormStep>
  );
}

export default ConfirmationStep;
