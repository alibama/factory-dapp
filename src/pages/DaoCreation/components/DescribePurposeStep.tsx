import { useTranslation } from 'react-i18next';

import { useFormArray } from '@q-dev/form-hooks';
import { Icon } from '@q-dev/q-ui-kit';
import { DescribePurposeFormParameter } from 'typings/forms';

import Button from 'components/Button';
import DescribePurposeForm from 'components/DescribePurposeForm';
import { FormStep } from 'components/MultiStepForm';

import { useDaoCreationForm } from '../DaoCreation';

function DescribePurposeStep () {
  const { t } = useTranslation();
  const { goNext, onChange } = useDaoCreationForm();
  const MAX_PURPOSES_COUNT = 10;

  const formArray = useFormArray<DescribePurposeFormParameter>({
    minCount: 1,
    maxCount: MAX_PURPOSES_COUNT,
    onSubmit: (forms) => {
      onChange({ describePurposes: forms });
      goNext();
    },
  });

  return (
    <FormStep
      disabled={!formArray.isValid}
      onNext={formArray.submit}
    >
      {formArray.forms.map((formItem, index) => (
        <DescribePurposeForm
          key={formItem.id}
          count={index + 1}
          length={formArray.forms.length}
          onRemove={() => formArray.removeForm(formItem.id)}
          onChange={formItem.onChange}
        />
      ))}

      {formArray.forms.length < MAX_PURPOSES_COUNT &&
        <Button
          look="ghost"
          onClick={formArray.appendForm}
        >
          <Icon name="add" />
          <span>{t('ADD_PURPOSE')}</span>
        </Button>}
    </FormStep>
  );
}

export default DescribePurposeStep;
