import { createContext, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { useMultiStepForm } from '@q-dev/form-hooks';
import { DaoCreationForm } from 'typings/forms';

import MultiStepForm from 'components/MultiStepForm';

import ConfirmationStep from './components/ConfirmationStep';
import CustomizeVetoStep from './components/CustomizeVetoStep';
import DescribePurposeStep from './components/DescribePurposeStep';
import ExpertPanelStep from './components/ExpertPanelStep';
import GovernanceStructureStep from './components/GovernanceStructureStep';
import TreasuryStep from './components/TreasuryStep';
import VetoPowersStep from './components/VetoPowersStep';
import VotingTokenStep from './components/VotingTokenStep';

import { useDaoCreate } from 'store/dao/hooks';

import { RoutePaths } from 'constants/routes';

const DEFAULT_VALUES: DaoCreationForm = {
  describePurposes: [],
  tokenType: 'native',
  customErc20Token: null,
  customErc721Token: null,
  tokenAddress: '',
  expertPanelRoles: [],
  isSetConstitutionVote: true,
  isSetGeneralUpdateVote: true,
  governanceSpecificRoles: [],
  isWithTreasury: false,
  treasuryAddress: '',
  vetoPowerType: 'nothing',
  guardianExpertPanel: null,
  defaultVetoExpertPanel: null,
  isMembershipsVote: [],
  constitutionVote: '',
  generalUpdateVote: '',
  expertPanelVote: [],
  membershipVotes: [],
};

const DaoCreationContext = createContext(
  {} as ReturnType<typeof useMultiStepForm<typeof DEFAULT_VALUES>>
);

function DaoCreation () {
  const { t } = useTranslation();
  const history = useHistory();
  const { setDeployForm } = useDaoCreate();

  const form = useMultiStepForm({
    initialValues: DEFAULT_VALUES,
    onConfirm: (form) => {
      setDeployForm(form);
      history.push(RoutePaths.deployment);
    },
  });

  const steps = [
    {
      id: 'describe',
      title: t('DAO_DESCRIBE_STEP_TITLE'),
      children: <DescribePurposeStep />
    },
    {
      id: 'voting-token',
      title: t('VOTING_TOKEN_STEP_TITLE'),
      children: <VotingTokenStep />
    },
    {
      id: 'expert-panel',
      title: t('EXPERT_PANEL_STEP_TITLE'),
      children: <ExpertPanelStep />
    },
    {
      id: 'params',
      title: t('GOVERNANCE_STRUCTURE_STEP_TITLE'),
      children: <GovernanceStructureStep />
    },
    {
      id: 'treasure',
      title: t('TREASURE_STEP_TITLE'),
      children: <TreasuryStep />
    },
    {
      id: 'veto-powers',
      title: t('VETO_POWERS_STEP_TITLE'),
      children: <VetoPowersStep />
    },
    {
      id: 'customize-veto',
      title: t('CUSTOMIZE_VETO_STEP_TITLE'),
      children: <CustomizeVetoStep />
    },
    {
      id: 'confirm',
      title: t('CONFIRMATION'),
      tip: t('CONFIRMATION_TIP'),
      children: <ConfirmationStep />
    }
  ];

  return (
    <DaoCreationContext.Provider value={form}>
      <MultiStepForm stepIndex={form.stepIndex} steps={steps} />
    </DaoCreationContext.Provider>
  );
}

export const useDaoCreationForm = () => useContext(DaoCreationContext);

export default DaoCreation;
