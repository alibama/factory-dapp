import { useCallback, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { Tip } from '@q-dev/q-ui-kit';
import uniqueId from 'lodash/uniqueId';
import styled from 'styled-components';
import { ExpertPanelFormParameter } from 'typings/forms';

import Button from 'components/Button';
import CopyToClipboard from 'components/CopyToClipboard';
import PageLayout from 'components/PageLayout';

import useNetworkConfig from 'hooks/useNetworkConfig';

import DeployTx from './components/DeployTx';

import { useDaoCreate } from 'store/dao/hooks';
import { useTransaction } from 'store/transaction/hooks';
import { Transaction, TxStatus } from 'store/transaction/reducer';

import { RoutePaths } from 'constants/routes';

const StyledDashboard = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;

  .deployment__block {
    display: flex;
    flex-direction: column;
    gap: 8px;
  }

  .deployment__title {
    display: flex;
    justify-content: space-between;
    padding-left: 12px;
    font-size: 14px;
  }

  .deployment__block-header {
    padding-top: 8px;
    padding-bottom: 32px;
  }

  .deployment__body {
    display: flex;
    flex-direction: column;
    gap: 16px;
    padding-bottom: 32px;
  }

  .deployment__list {
    display: flex;
    flex-direction: column;
    gap: 16px;
  }

  .deployment__empty { 
    display: flex;
    justify-content: center;
    padding: 32px;
    height: 100%;
  }

  .deployment__block-actions {
    display: flex;
    justify-content: flex-end;
    gap: 8px;
  }

  .deployment__dao-registry {
    display: flex;
    gap: 4px;
  }
`;

export default function Deployment () {
  const { t } = useTranslation();
  const {
    daoForm,
    deployDaoParams,
    deployVetoParams,
    deployPanel,
    deployGuardian,
    setDeployForm,
    daoRegistryAddress,
    deployQRC20Token,
    deployQRC721Token
  } = useDaoCreate();
  const { submitTransaction, transactions, resetTransactionsStore } = useTransaction();
  const history = useHistory();
  const { dashboardBaseUrl } = useNetworkConfig();

  const hasTxError = useMemo(() =>
    transactions.find((transaction: Transaction) => transaction.status === 'error'), [transactions]
  );

  const deployTx = useMemo(() => {
    return daoForm
      ? {
        ...(daoForm.customErc20Token || daoForm.customErc721Token) && {
          token: {
            id: uniqueId(),
            message: t('DEPLOY_TOKEN', { name: daoForm.customErc20Token?.symbol || daoForm.customErc721Token?.symbol }),
            status: 'pending' as TxStatus
          }
        },
        dao: {
          id: uniqueId(),
          message: t('DEPLOY_DAO'),
          status: 'pending' as TxStatus
        },
        panels: [
          ...daoForm?.expertPanelRoles.map((item: ExpertPanelFormParameter) => {
            return {
              id: uniqueId(),
              status: 'pending' as TxStatus,
              message: t('DEPLOY_PANEL', { name: item.name })
            };
          }),
          ...(daoForm.guardianExpertPanel
            ? [{
              id: uniqueId(),
              message: t('DEPLOY_PANEL', { name: daoForm.guardianExpertPanel.name }),
              status: 'pending' as TxStatus
            }]
            : []),
        ],
        veto: {
          id: uniqueId(),
          message: t('DEPLOY_VETO_PARAMS'),
          status: 'pending' as TxStatus
        }
      }
      : null;
  }, []);

  const flattenedTxIds = useMemo(() => deployTx ? Object.values(deployTx).flat() : [], [deployTx]) as Transaction[];

  const deployPanels = async () => {
    if (!daoForm || !deployTx) return;
    const panelsToDeploy = [
      ...daoForm.expertPanelRoles,
      ...(daoForm.guardianExpertPanel ? [daoForm.guardianExpertPanel] : [])
    ];
    await Promise.all(panelsToDeploy.map(async (_, i) => {
      return await submitTransaction({
        id: deployTx.panels[i].id,
        successMessage: deployTx.panels[i].message,
        submitFn: async () =>
          daoForm.guardianExpertPanel && panelsToDeploy.length - 1 === i
            ? await deployGuardian()
            : await deployPanel(i),
      });
    }));
    submitTransaction({
      id: deployTx.veto.id,
      successMessage: deployTx.veto.message,
      submitFn: () => deployVetoParams(),
    });
  };

  useEffect(() => {
    if (!daoForm) {
      history.push(RoutePaths.dashboard);
      return;
    };
    deployTx?.token
      ? submitTransaction({
        id: deployTx.token.id,
        successMessage: deployTx.token.message,
        submitFn: () => daoForm.customErc20Token
          ? deployQRC20Token(deployTx.token?.id)
          : deployQRC721Token(deployTx.token?.id),
        onSuccess: () => deployDao(),
      })
      : deployDao();
  }, []);

  const deployDao = useCallback(async () => {
    await submitTransaction({
      id: deployTx?.dao.id,
      successMessage: deployTx?.dao.message,
      submitFn: () => deployDaoParams(deployTx?.dao.id),
      onSuccess: () => deployPanels(),
    });
  }, []);

  useEffect(() => () => {
    setDeployForm(null);
    resetTransactionsStore();
  }, []);

  return (
    <PageLayout>
      <StyledDashboard>
        <h3 className="deployment__title">
          {t('DAO_CREATION')}
        </h3>
        <div className="deployment__block block">
          <h3 className="deployment__block-header text-h2">
            {t('USER_TRANSACTIONS')}
          </h3>
          <div className="deployment__body">
            {flattenedTxIds?.length
              ? (
                <div className="deployment__list">
                  {flattenedTxIds.map((transaction: Transaction) => (
                    <DeployTx
                      key={transaction.id}
                      tx={transaction}
                    />
                  ))}
                </div>
              )
              : (
                <p className="deployment__empty color-secondary text-md">
                  {t('EMPTY_TX')}
                </p>
              )
            }
            {daoRegistryAddress && (
              <Tip type="info">
                <div className="deployment__dao-registry">
                  <span className="text-md">
                    {t('DAO_REGISTRY_ADDRESS')}
                  </span>
                  <span className="text-md font-semibold">
                    {daoRegistryAddress}
                    <CopyToClipboard value={daoRegistryAddress} />
                  </span>
                </div>
              </Tip>
            )}
            {hasTxError && (
              <Tip type="warning">
                {t('SOMETHING_WENT_WRONG')}
              </Tip>
            )}
          </div>

          <div className="deployment__block-actions">
            <Button
              look="ghost"
              onClick={() => history.push(RoutePaths.dashboard)}
            >
              <span>{t('GO_TO_DAO_CREATION')}</span>
            </Button>

            <Link
              target="_blank"
              rel="noopener noreferer"
              to={{ pathname: `${dashboardBaseUrl}/${daoRegistryAddress}` }}
            >
              <Button disabled={!daoRegistryAddress}>
                <span>{t('GO_TO_DAO_DASHBOARD')}</span>
              </Button>
            </Link>
          </div>
        </div>
      </StyledDashboard>
    </PageLayout >
  );
}
