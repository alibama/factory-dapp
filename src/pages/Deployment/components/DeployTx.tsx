import { useMemo } from 'react';

import { Icon, Spinner } from '@q-dev/q-ui-kit';
import styled from 'styled-components';

import useNetworkConfig from 'hooks/useNetworkConfig';

import { useTransaction } from 'store/transaction/hooks';
import { Transaction, TxStatus } from 'store/transaction/reducer';

const DeployTxContainer = styled.div<{ txState: TxStatus }>`
  .deploy-tx__item {
    display: flex;
    align-items: center;
    background-color: transparent;
    padding: 12px;
    gap: 24px;
    width: 100%;
    border-radius: 8px;
    border: 1px solid ${({ theme }) => theme.colors.borderAdditional};
    
    &:hover {
      background-color: ${({ theme }) => theme.colors.tertiaryLight};
    }
  }

  .deploy-tx__item-title {
    display: block;
    width: 100%;
    font-weight: 600;
    white-space: break-spaces;
    word-wrap: break-word;
  }

  .deploy-tx__item-icon {
    font-size: 40px;
    color: ${({ theme, txState }) => {
    switch (txState) {
      case 'success':
        return theme.colors.successMain;
      case 'error':
        return theme.colors.errorMain;
      case 'pending':
      case 'sending':
        return theme.colors.infoPrimary;
    }
  }};
  }

  .deploy-tx__item-icon-wrapper {
    padding: 12px;
    width: 64px;
    height: 64px;
    border-radius: 4px;
    background-color: ${({ theme, txState }) => {
    switch (txState) {
      case 'success':
        return theme.colors.primaryDark;
      case 'error':
        return theme.colors.errorTertiary;
      case 'pending':
      case 'sending':
        return theme.colors.tertiaryMain;
    }
  }};
  }
`;

interface Props {
  tx: Transaction;
}

function DeployTx ({ tx }: Props) {
  const { explorerUrl } = useNetworkConfig();
  const { transactions } = useTransaction();

  const transaction = useMemo(() =>
    transactions.find((transaction: Transaction) => tx.id === transaction.id) || tx, [transactions]
  );

  const isTxCompleted = useMemo(() =>
    transaction?.status === 'success' || transaction?.status === 'error', [transaction.status]
  );

  const iconName = useMemo(() =>
    transaction?.status === 'success' ? 'double-check' : 'cross', [transaction.status]
  );

  return (
    <DeployTxContainer txState={transaction.status}>
      {transaction.hash
        ? (
          <a
            href={`${explorerUrl}/tx/${transaction.hash}`}
            target="_blank"
            className="deploy-tx__item text-md"
            rel="noreferrer"
            title={transaction.message}
          >
            <div className="deploy-tx__item-icon-wrapper">
              {isTxCompleted
                ? <Icon name={iconName} className="deploy-tx__item-icon" />
                : <Spinner size={40} />
              }
            </div>
            <span className="deploy-tx__item-title">
              {transaction.message}
            </span>
          </a>
        )
        : (
          <div
            className="deploy-tx__item text-md"
            title={transaction.message}
          >
            <div className="deploy-tx__item-icon-wrapper">
              {isTxCompleted
                ? <Icon name={iconName} className="deploy-tx__item-icon" />
                : <Spinner size={40} />
              }
            </div>
            <span className="deploy-tx__item-title">
              {transaction.message}
            </span>
          </div>)
      }
    </DeployTxContainer>
  );
}

export default DeployTx;
