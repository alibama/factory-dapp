import { lazy, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';

import * as Sentry from '@sentry/react';
import { useWeb3Context } from 'context/Web3ContextProvider';

import LazyLoading from 'components/Base/LazyLoading';
import ErrorBoundary from 'components/Custom/ErrorBoundary';

import { captureError } from 'utils';

const Dashboard = lazy(() => import('pages/Dashboard'));
const DataPrivacy = lazy(() => import('pages/DataPrivacy'));
const Imprint = lazy(() => import('pages/Imprint'));
const NotFound = lazy(() => import('pages/NotFound'));
const DaoCreation = lazy(() => import('pages/DaoCreation'));
const Deployment = lazy(() => import('pages/Deployment'));

function Routes () {
  const { currentProvider } = useWeb3Context();
  const addSentryContext = () => {
    try {
      if (!currentProvider) return;
      Sentry.setContext('additional', { network: currentProvider.chainId });
    } catch (error) {
      captureError(error);
    }
  };

  useEffect(() => {
    addSentryContext();
  }, [currentProvider]);

  return (
    <ErrorBoundary>
      <LazyLoading>
        <Switch>
          <Route exact path={['/']}>
            <Dashboard />
          </Route>

          <Route exact path="/dao">
            <DaoCreation />
          </Route>

          <Route exact path="/deployment">
            <Deployment />
          </Route>

          <Route exact path="/imprint">
            <Imprint />
          </Route>
          <Route exact path="/data-privacy">
            <DataPrivacy />
          </Route>

          <Route component={NotFound} />
        </Switch>
      </LazyLoading>
    </ErrorBoundary>
  );
}

export default Routes;
