import { media } from '@q-dev/q-ui-kit';
import styled from 'styled-components';

export const SidebarContainer = styled.div<{ $open: boolean }>`
  .sidebar {
    display: flex; 
    width: 302px;
    height: 100vh;
    display: grid;
    grid-template-rows: minmax(0, 1fr) auto; 
    align-content: space-between;
    gap: 24px;
    padding: 16px 32px;
    background-color: ${({ theme }) => theme.colors.backgroundPrimary};
    border-right: 1px solid ${({ theme }) => theme.colors.borderPrimary};
    position: fixed;
    z-index: 9999;
    transform: translateX(${({ $open }) => ($open ? '0' : '-100%')}) scaleX(${({ $open }) => $open ? '1' : '0.8'});
    transform-origin: top left;
    transition: transform 200ms ease-out;
    padding: 16px 24px;
  }

  .sidebar__overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: ${({ theme }) => theme.colors.backdropOverlay};
    z-index: 1;
    display: block;
    opacity: ${({ $open }) => $open ? '0.5' : '0'};
    pointer-events: ${({ $open }) => $open ? 'all' : 'none'};
    transition: opacity 200ms ease-out;
  }

  .sidebar__content {
    display: grid;
    grid-template-rows: auto minmax(0, 1fr);
    align-content: start;
    gap: 16px;
  }

  .sidebar__logo-link {
    display: flex;
    gap: 8px;
    width: max-content;
  }

  .sidebar__logo {
    width: 36px;
    height: 32px;
  }


  .sidebar__project-name {
    font-size: 24px;
  }
  
  .sidebar__main {
    display: grid;
    gap: 16px;
    align-content: start;
  }

  .sidebar__links {
    display: none;
    gap: 4px;
    overflow-y: auto;
    overflow-x: hidden;
    // HACK: Display scrollbar inside the container
    margin: 0 -16px;
    padding: 0 16px;

    ${media.lessThan('medium')} {
      display: grid;
    }
  }

  .sidebar__links-item{ 
    padding: 16px 0;
  }

  .sidebar__footer {
    display: flex;
    gap: 8px;
    justify-content: center;
    flex-wrap: wrap;
  }

  .sidebar__footer-link {
    color: ${({ theme }) => theme.colors.textSecondary};
    background-color: transparent;
    border: none;
    outline: none;
    padding: 0;

    &:hover,
    &:focus-visible {
      color: ${({ theme }) => theme.colors.textPrimary};
      text-decoration: underline;
    }

    &:not(:last-child) {
      padding-right: 8px;
      border-right: 1px solid ${({ theme }) => theme.colors.borderSecondary};
    }
  }

  // TODO: Remove when aliasing link is removed from sidebar
  ${media.lessThan('huge')} {
    gap: 16px;

    .sidebar__main {
      gap: 8px;
    }
  }
`;
