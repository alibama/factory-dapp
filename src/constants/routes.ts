export enum RoutePaths {
  dashboard = '/',
  deployment = '/deployment',
  daoForm = '/dao',
}
