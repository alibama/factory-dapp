import { utils } from 'ethers';
import { Chain } from 'typings';

export type NetworkName = 'mainnet' | 'testnet' | 'devnet';

interface NetworkConfig {
  chainId: number;
  name: string;
  networkName: NetworkName;
  dAppUrl: string;
  rpcUrl: string;
  indexerUrl: string;
  explorerUrl: string;
  gnosisSafeUrl: string;
  qBridgeUrl: string;
  docsUrl: string;
  constitutionUrl: string;
  gasBuffer: number;
  featureFlags: {
    aliases: boolean;
  };
  masterDaoFactory: string;
  masterContractRegistry: string;
  expertsDAOVotingAddrAddr: string;
  generalDAOVotingAddr: string;
  dashboardBaseUrl: string;
}

export const networkConfigsMap: Record<NetworkName, NetworkConfig> = {
  mainnet: {
    chainId: 35441,
    name: 'Q Mainnet',
    networkName: 'mainnet',
    dAppUrl: 'https://hq.q.org',
    rpcUrl: 'https://rpc.q.org',
    indexerUrl: 'https://indexer.q.org',
    explorerUrl: 'https://explorer.q.org',
    gnosisSafeUrl: 'https://multisig-ui.q.org',
    qBridgeUrl: 'https://bridge.q.org',
    docsUrl: 'https://docs.q.org',
    constitutionUrl: 'https://constitution.q.org',
    gasBuffer: 1.5,
    featureFlags: { aliases: true },
    masterDaoFactory: '0xb83Ca05D2292a61DCa92E0a7cA9a34a7Daf7F2A0',
    masterContractRegistry: '0x73822fA7417f7EC4E35098442b8Fe3831E4FcD3C',
    generalDAOVotingAddr: '0xCEA2a40Fc0c632fCB61CDa1459e97cd2ce4cdEb4',
    expertsDAOVotingAddrAddr: '0x637a589c1E278c0e16f4982B23D7C3d4F6e9c060',
    dashboardBaseUrl: 'http://hq.q-dao.tools'
  },
  testnet: {
    chainId: 35443,
    name: 'Q Testnet',
    networkName: 'testnet',
    dAppUrl: 'https://hq.qtestnet.org',
    rpcUrl: 'https://rpc.qtestnet.org',
    indexerUrl: 'https://indexer.qtestnet.org',
    explorerUrl: 'https://explorer.qtestnet.org',
    gnosisSafeUrl: 'https://multisig.qtestnet.org',
    qBridgeUrl: 'https://bridge.qtestnet.org',
    docsUrl: 'https://docs.qtestnet.org',
    constitutionUrl: 'https://constitution.qtestnet.org',
    gasBuffer: 2,
    featureFlags: { aliases: true },
    masterDaoFactory: '0x66743Cf4EA181D8C9AfC9C7e19DBffBBb1a9102d',
    masterContractRegistry: '0x87529967a184BC4d6863B9CBe8b2C96CaA05404E',
    generalDAOVotingAddr: '0x9EAdc15A08F121E36af1C2826B6d2845B34184AE',
    expertsDAOVotingAddrAddr: '0x7ab8700D24c34564B3ED47bbE3d258f9C98eB73e',
    dashboardBaseUrl: 'http://hq.q-dao.tools'
  },
  devnet: {
    chainId: 35442,
    name: 'Q Devnet',
    networkName: 'devnet',
    dAppUrl: 'https://hq.qdevnet.org',
    rpcUrl: 'https://rpc.qdevnet.org',
    indexerUrl: 'https://indexer.qdevnet.org',
    explorerUrl: 'https://explorer.qdevnet.org',
    gnosisSafeUrl: 'https://multisig.qdevnet.org',
    qBridgeUrl: 'https://bridge.qdevnet.org',
    docsUrl: 'https://docs.qtestnet.org',
    constitutionUrl: 'https://constitution.qdevnet.org',
    gasBuffer: 1.5,
    featureFlags: { aliases: true },
    masterDaoFactory: '0x507B61953B241ba4451cBfbFA588c886142872Ef',
    masterContractRegistry: '0x5521A9aC52F7550F5b624Fb2646f9131f554e2a1',
    generalDAOVotingAddr: '0x41B15ee16136b012311E84bf8C1700bd37450047',
    expertsDAOVotingAddrAddr: '0x838BaB22a3a24719FCA51838d23A6f54eD159Fc9',
    dashboardBaseUrl: 'http://hq.q-dao.tools'
  },
};

export const chainIdToNetworkMap: { [key: string]: NetworkName } = {
  35441: 'mainnet',
  35442: 'devnet',
  35443: 'testnet',
};

export const connectorParametersMap = Object.values(networkConfigsMap)
  .reduce((acc, config) => {
    acc[config.chainId] = {
      chainId: utils.hexlify(config.chainId),
      chainName: config.name,
      rpcUrls: [config.rpcUrl],
      blockExplorerUrls: [config.explorerUrl],
      nativeCurrency: {
        name: 'Q',
        // HACK: MetaMask requires the symbol to have at least 2 characters
        symbol: 'Q ',
        decimals: 18,
      },
    };
    return acc;
  }, {} as { [key: string]: Chain });

const originToNetworkMap: { [key: string]: NetworkName } = {
  'https://hq.q.org': 'mainnet',
  'https://hq.qtestnet.org': 'testnet',
  'https://hq.qdevnet.org': 'devnet',
  'http://localhost:3000': 'devnet',
};

export const ORIGIN_NETWORK_NAME: NetworkName = originToNetworkMap[window.location.origin] || 'devnet';
