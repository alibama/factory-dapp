import { BaseContractInstance } from '@q-dev/gdk-sdk/lib/contracts/BaseContractInstance';
import { DAORegistryInstance } from '@q-dev/gdk-sdk/lib/contracts/DAO/DAORegistryInstance';

type KeyOfType<T, U> = {
  [P in keyof T]: T[P] extends U ? P: never
}[keyof T];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ContractPromise = Promise<BaseContractInstance<any>>;
export type ContractType = KeyOfType<DAORegistryInstance, (val: string) => ContractPromise>;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ContractValue<T extends ContractType = any> = ReturnType<DAORegistryInstance[T]>;
