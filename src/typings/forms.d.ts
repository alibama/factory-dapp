interface TreasuryFormParameter {
  isWithTreasury: boolean;
  treasuryAddress: string;
}

interface ExpertPanelFormParameter {
  name: string;
  purpose: string;
}

interface GovernanceStructureFormParameter {
  isGeneraleUpdateVote: boolean;
  isParameterVote: boolean;
}

interface MemberVoteFormParameter {
  isMembershipVote: boolean;
}

interface CustomExpertPanelVoteFormParameter {
  generalUpdateVote: string;
  parameterVote: string;
}

interface GeneralMembersVoteFormParameter {
  membershipVote: string;
}

interface DescribePurposeFormParameter {
  description: string;
}

interface CustomErc20Token {
  name: string;
  decimals: string;
  symbol: string;
  totalSupplyCap: string;
  contractURI: string;
}

interface CustomErc721Token {
  name: string;
  symbol: string;
  contractURI: string;
  baseURI: string;
  totalSupplyCap: number;
  burnable: boolean;
  transferable: boolean;
  }

interface FormDelegation {
  address: string;
  amount: string;
}

type VotingTokenType = 'native' | 'existing-token' | 'erc20' | 'erc721';
type VetoPowerType = 'yes' | 'new-role' | 'nothing';

interface VotingTokenFormParameter {
  tokenType: VotingTokenType;
  customErc20TokenAddress: string;
}

interface VetoPowersFormParameter {
  vetoPowerType: VetoPowerType;
  guardianExpertPanelIndex: number;
}

export interface DaoCreationForm {
  describePurposes: DescribePurposeFormParameter[];
  tokenType: VotingTokenType;
  customErc20Token: CustomErc20Token | null;
  customErc721Token: CustomErc721Token | null;
  tokenAddress?: string;
  expertPanelRoles: ExpertPanelFormParameter[];
  governanceSpecificRoles: GovernanceStructureFormParameter[];
  isSetConstitutionVote: boolean;
  isSetGeneralUpdateVote: boolean;
  isMembershipsVote: MemberVoteFormParameter[];
  guardianExpertPanel: ExpertPanelFormParameter | null;
  defaultVetoExpertPanel: ExpertPanelFormParameter | null;
  treasuryAddress: string;
  isWithTreasury: boolean;
  vetoPowerType: VetoPowerType;
  constitutionVote: string;
  generalUpdateVote: string;
  expertPanelVote: CustomExpertPanelVoteFormParameter[];
  membershipVotes: GeneralMembersVoteFormParameter[];
}
