
import { useMemo } from 'react';

import { useWeb3Context } from 'context/Web3ContextProvider';

import useNetworkConfig from './useNetworkConfig';

import { getMasterContractsRegistryInstance, getMasterDaoFactoryInstance } from 'contracts/contract-instance';

function useGdkContract () {
  const { currentProvider } = useWeb3Context();
  const networkConfig = useNetworkConfig();

  const masterDaoFactoryInstance = useMemo(() => {
    return currentProvider?.currentSigner
      ? getMasterDaoFactoryInstance(
        networkConfig.masterDaoFactory,
        currentProvider.currentSigner
      )
      : undefined;
  }, [currentProvider, networkConfig]);

  const masterContractsRegistryInstance = useMemo(() => {
    return currentProvider?.currentSigner
      ? getMasterContractsRegistryInstance(
        networkConfig.masterContractRegistry,
        currentProvider.currentSigner
      )
      : undefined;
  }, [currentProvider, networkConfig]);

  return {
    masterDaoFactoryInstance,
    masterContractsRegistryInstance
  };
}

export default useGdkContract;
