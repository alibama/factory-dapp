import { useCallback } from 'react';
import { useAlert } from 'react-alert';
import { useDispatch } from 'react-redux';

import { ContractTransaction } from 'ethers';
import { t } from 'i18next';
import uniqueId from 'lodash/uniqueId';

import { setTransactions, Transaction, TransactionEditableParams } from './reducer';

import { getState, useAppSelector } from 'store';

import { captureError } from 'utils/errors';

export function useTransaction () {
  const dispatch = useDispatch();
  const alert = useAlert();
  const transactions = useAppSelector(({ transaction }) => transaction.transactions);

  async function submitTransaction ({
    id = uniqueId(),
    submitFn,
    successMessage,
    onSuccess = () => {},
    onConfirm = () => {},
    onError = () => {},
  }: {
    submitFn: () => Promise<ContractTransaction | void>;
    successMessage?: string;
    id?: string;
    onSuccess?: () => void | Promise<void>;
    onConfirm?: () => void;
    onError?: (error?: unknown) => void;
  }) {
    const transaction: Transaction = {
      id,
      message: successMessage || t('DEFAULT_MESSAGE_TX'),
      status: 'pending',
    };

    const { transactions } = getState().transaction;
    dispatch(setTransactions([{ ...transaction }, ...transactions]));
    try {
      const submitResponse = await submitFn();
      if (submitResponse?.wait) {
        updateTransaction(transaction.id, { hash: submitResponse.hash, status: 'sending' });
        onConfirm();
        await submitResponse.wait();
      }
      updateTransaction(transaction.id, { status: 'success' });
      alertTxStatus(transaction.id, 'success', transaction.message);
      await onSuccess();
    } catch (error) {
      captureError(error);
      onError(error);
      updateTransaction(transaction.id, { status: 'error' });
      alertTxStatus(transaction.id, 'error', getErrorMessage(error));
    }
  }

  const getTxById = (id: string) => {
    const { transactions } = getState().transaction;
    return transactions.find((tx: Transaction) => tx.id === id);
  };

  const updateTransaction = (id: string, params: TransactionEditableParams) => {
    const { transactions } = getState().transaction;
    const txIndex = transactions.findIndex((tx: Transaction) => tx.id === id);
    if (txIndex === -1) return;
    const newTxs = [...transactions];
    newTxs[txIndex] = { ...newTxs[txIndex], ...params };
    dispatch(setTransactions(newTxs));
  };

  const resetTransactionsStore = () => {
    dispatch(setTransactions([]));
  };

  const alertTxStatus = (id: string, type: 'success' | 'error', message: string) => {
    const currentTx = getTxById(id);
    if (currentTx?.isClosedModal) {
      alert[type](message);
    }
  };

  return {
    transactions,
    submitTransaction: useCallback(submitTransaction, []),
    updateTransaction: useCallback(updateTransaction, []),
    resetTransactionsStore: useCallback(resetTransactionsStore, []),
  };
}

function getErrorMessage (err: unknown): string {
  const error = err as {
    message: string;
    code?: number;
    stack?: string;
  };

  if (error.code === 4001) {
    return t('ERROR_TRANSACTION_REJECTED');
  }

  if (!error.message?.includes('Internal JSON-RPC error')) {
    if (error.message?.includes('Transaction has been reverted by the EVM')) {
      return t('ERROR_TRANSACTION_REVERTED_BY_EVM');
    }

    return error.message || t('ERROR_UNKNOWN');
  }

  if (error.message === 'execution reverted') {
    return t('ERROR_TRANSACTION_REVERTED');
  }

  const rpcErrorCode = error.message.match(/\[.+-(.+)\]/)?.at(1);
  return rpcErrorCode
    ? t(`ERROR_${rpcErrorCode}`)
    : t('ERROR_RPC_UNKNOWN');
}
