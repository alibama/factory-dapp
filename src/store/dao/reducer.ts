import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DaoCreationForm } from 'typings/forms';

interface DaoCreateState {
  daoForm: DaoCreationForm | null;
  daoRegistryAddress: string;
}

const initialState: DaoCreateState = {
  daoForm: null,
  daoRegistryAddress: ''
};

const daoSlice = createSlice({
  name: 'dao',
  initialState,
  reducers: {
    setDaoForm: (state, { payload }: PayloadAction<DaoCreationForm | null>) => {
      state.daoForm = payload;
    },
    setDaoRegistryAddress: (state, { payload }: PayloadAction<string>) => {
      state.daoRegistryAddress = payload;
    },
  },

});

export const { setDaoForm, setDaoRegistryAddress } = daoSlice.actions;
export default daoSlice.reducer;
