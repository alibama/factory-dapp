import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import {
  DAO_MEMBER_STORAGE_NAME,
  DAO_PANEL_LIMIT_NAME,
  DAO_PARAMETER_STORAGE_NAME,
  DAO_RESERVED_NAME,
  ETHEREUM_ADDRESS,
  EXPERTS_VOTING_NAME,
  GENERAL_VOTING_NAME,
  IPermissionManager,
  ParameterType,
} from '@q-dev/gdk-sdk';
import { toBigNumber } from '@q-dev/utils';
import { useWeb3Context } from 'context/Web3ContextProvider';
import { CustomExpertPanelVoteFormParameter, DaoCreationForm, GeneralMembersVoteFormParameter } from 'typings/forms';

import useGdkContract from 'hooks/useGdkContract';
import useNetworkConfig from 'hooks/useNetworkConfig';

import { setDaoForm, setDaoRegistryAddress } from './reducer';

import { getState, useAppSelector } from 'store';
import { useTransaction } from 'store/transaction/hooks';

import {
  getDaoRegistryInstance,
  getTokenFactoryInstance
} from 'contracts/contract-instance';

import { BYTES_10, DEFAULT_CONTRACT_URI, MAX_BLOCK_GAS_LIMIT, RAND_BYTES_32 } from 'constants/contracts';

const FIVE_PERCENTAGE = '50000000000000000000000000';
const FIFTY_PERCENTAGE = '500000000000000000000000000';

export function useDaoCreate () {
  const { currentProvider } = useWeb3Context();

  const { updateTransaction } = useTransaction();
  const { masterDaoFactoryInstance, masterContractsRegistryInstance } = useGdkContract();
  const dispatch = useDispatch();

  const daoForm = useAppSelector(({ dao }) => dao.daoForm);
  const daoRegistryAddress = useAppSelector(({ dao }) => dao.daoRegistryAddress);

  const networkConfig = useNetworkConfig();

  const setDeployForm = async (form: DaoCreationForm | null) => {
    dispatch(setDaoForm(form));
    dispatch(setDaoRegistryAddress(''));
  };

  const deployDaoParams = async (txId?: string) => {
    const { daoForm } = getState().dao;
    if (!daoForm || !masterDaoFactoryInstance) return;
    const daoParams = {
      masterAccess: currentProvider.selectedAddress,
      votingNames: [GENERAL_VOTING_NAME, EXPERTS_VOTING_NAME],
      votingAddresses: [networkConfig.generalDAOVotingAddr, networkConfig.expertsDAOVotingAddrAddr],
      mainPanelParams: {
        votingParams: {
          votingToken: daoForm.tokenAddress || ETHEREUM_ADDRESS,
          panelName: 'DAO Constitution',
          situations: [
            {
              votingSituationName: 'General Update Vote',
              votingValues: {
                votingPeriod: 300,
                vetoPeriod: 300,
                proposalExecutionPeriod: 86400,
                requiredQuorum: FIVE_PERCENTAGE,
                requiredMajority: FIFTY_PERCENTAGE,
                requiredVetoQuorum: FIFTY_PERCENTAGE,
                votingType: 0,
                votingTarget: `${GENERAL_VOTING_NAME}:${DAO_RESERVED_NAME}`,
                votingMinAmount: 10,
              },
            },
            {
              votingSituationName: 'Constitution Vote',
              votingValues: {
                votingPeriod: 300,
                vetoPeriod: 300,
                proposalExecutionPeriod: 86400,
                requiredQuorum: FIVE_PERCENTAGE,
                requiredMajority: FIFTY_PERCENTAGE,
                requiredVetoQuorum: FIFTY_PERCENTAGE,
                votingType: 0,
                votingTarget: `${DAO_PARAMETER_STORAGE_NAME}:${DAO_RESERVED_NAME}`,
                votingMinAmount: 1,
              },
            },
          ],
        },
        initialMembers: [],
        initialParameters: [
          {
            name: 'constitution.hash',
            value: RAND_BYTES_32,
            solidityType: ParameterType.BYTES
          },
          {
            name: DAO_PANEL_LIMIT_NAME,
            value: BYTES_10,
            solidityType: ParameterType.UINT256,
          }
        ],
      },
    };

    const deployDAOReceipt = await (await masterDaoFactoryInstance.deployDAO(
      daoParams,
      { gasLimit: MAX_BLOCK_GAS_LIMIT }
    )).wait();
    if (txId && deployDAOReceipt) {
      updateTransaction(txId, { hash: deployDAOReceipt.transactionHash });
      dispatch(setDaoRegistryAddress(masterDaoFactoryInstance.getDAORegistryAddressFromTx(deployDAOReceipt)));
    }
  };

  const deployPanel = async (i: number) => {
    const { daoRegistryAddress, daoForm } = getState().dao;
    if (!daoRegistryAddress || !daoForm || !masterDaoFactoryInstance) return;
    const expertPanel = {
      votingParams: {
        votingToken: daoForm.tokenAddress || ETHEREUM_ADDRESS,
        panelName: daoForm.expertPanelRoles[i].name,
        situations: [
          ...daoForm.governanceSpecificRoles[i].isGeneraleUpdateVote
            ? [{
              votingSituationName: 'General Update Vote',
              votingValues: {
                votingPeriod: 300,
                vetoPeriod: 300,
                proposalExecutionPeriod: 86400,
                requiredQuorum: FIVE_PERCENTAGE,
                requiredMajority: FIFTY_PERCENTAGE,
                requiredVetoQuorum: FIFTY_PERCENTAGE,
                votingType: 1,
                votingTarget: `${EXPERTS_VOTING_NAME}:${daoForm.expertPanelRoles[i].name}`,
                votingMinAmount: 1,
              },
            }]
            : [],
          ...daoForm.governanceSpecificRoles[i].isParameterVote
            ? [{
              votingSituationName: 'Parameter Vote',
              votingValues: {
                votingPeriod: 300,
                vetoPeriod: 300,
                proposalExecutionPeriod: 86400,
                requiredQuorum: FIVE_PERCENTAGE,
                requiredMajority: FIFTY_PERCENTAGE,
                requiredVetoQuorum: FIFTY_PERCENTAGE,
                votingType: 1,
                votingTarget: `${DAO_PARAMETER_STORAGE_NAME}:${daoForm.expertPanelRoles[i].name}`,
                votingMinAmount: 1,
              },
            }]
            : [],
          ...daoForm.isMembershipsVote[i].isMembershipVote
            ? [{
              votingSituationName: 'Membership Vote',
              votingValues: {
                votingPeriod: 300,
                vetoPeriod: 300,
                proposalExecutionPeriod: 86400,
                requiredQuorum: FIVE_PERCENTAGE,
                requiredMajority: FIFTY_PERCENTAGE,
                requiredVetoQuorum: FIFTY_PERCENTAGE,
                votingType: 0,
                votingTarget: `${DAO_MEMBER_STORAGE_NAME}:${daoForm.expertPanelRoles[i].name}`,
                votingMinAmount: 1,
              }
            }]
            : [],
        ]
      },
      initialMembers: [],
      initialParameters: [],
    };

    return masterDaoFactoryInstance.deployDAOPanel(
      daoRegistryAddress,
      expertPanel,
      { gasLimit: MAX_BLOCK_GAS_LIMIT }
    );
  };

  const deployGuardian = async () => {
    const { daoRegistryAddress, daoForm } = getState().dao;
    if (!daoForm?.guardianExpertPanel || !daoRegistryAddress || !masterDaoFactoryInstance) return;
    const guardianExpertPanel = {
      votingParams: {
        votingToken: daoForm.tokenAddress || ETHEREUM_ADDRESS,
        panelName: daoForm.guardianExpertPanel.name,
        situations: [],
      },
      initialMembers: [],
      initialParameters: [],
    };

    return masterDaoFactoryInstance.deployDAOPanel(
      daoRegistryAddress,
      guardianExpertPanel,
      { gasLimit: MAX_BLOCK_GAS_LIMIT });
  };

  const deployVetoParams = async () => {
    const { daoRegistryAddress, daoForm } = getState().dao;
    if (!daoForm || !daoRegistryAddress || !masterDaoFactoryInstance || !currentProvider.currentSigner) return;

    const daoRegistryInstance = getDaoRegistryInstance(daoRegistryAddress, currentProvider.currentSigner);

    const membershipVetoGroups = await Promise.all(
      daoForm.membershipVotes.map(async (item: GeneralMembersVoteFormParameter, i: number) => {
        const expertPanelName = daoForm.expertPanelRoles[i].name;
        return (
          item.membershipVote &&
          daoForm.isMembershipsVote[i].isMembershipVote &&
         daoRegistryInstance &&
         item.membershipVote !== 'default-no-veto'
            ? {
              target: await daoRegistryInstance.instance.getDAOMemberStorage(expertPanelName),
              name: `Veto ${expertPanelName} Membership Vote`,
              initialMembers: [],
              linkedMemberStorage: await daoRegistryInstance.instance
                .getDAOMemberStorage(item.membershipVote),
            }
            : null);
      })
    );

    const expertPanelVetoGroups = await Promise.all(
      daoForm.expertPanelVote.map(async (item: CustomExpertPanelVoteFormParameter, i: number) => {
        if (!daoRegistryInstance) return [];
        const expertPanelName = daoForm.expertPanelRoles[i].name;
        const parameterVote = item.parameterVote &&
        item.parameterVote !== 'default-no-veto' &&
        daoForm.governanceSpecificRoles[i]?.isParameterVote
          ? {
            target: await daoRegistryInstance.instance
              .getDAOParameterStorage(expertPanelName),
            name: `Veto ${expertPanelName} Parameter Vote`,
            initialMembers: [],
            linkedMemberStorage: await daoRegistryInstance.instance
              .getDAOMemberStorage(item.parameterVote),
          }
          : null;
        const generalUpdateVote = item.generalUpdateVote &&
        item.generalUpdateVote !== 'default-no-veto' &&
        daoForm.governanceSpecificRoles[i]?.isGeneraleUpdateVote
          ? {
            target: await daoRegistryInstance.instance.getExpertsDAOVoting(expertPanelName),
            name: `Veto ${expertPanelName} General Update Vote`,
            initialMembers: [],
            linkedMemberStorage: await daoRegistryInstance.instance
              .getDAOMemberStorage(item.generalUpdateVote)
          }
          : null;
        return [parameterVote, generalUpdateVote];
      }));

    const constitutionVeto = daoForm.constitutionVote && daoForm.constitutionVote !== 'default-no-veto'
      ? {
        target: await daoRegistryInstance.instance.getDAOParameterStorage(DAO_RESERVED_NAME),
        name: 'Veto Constitution Vote',
        initialMembers: [],
        linkedMemberStorage: await daoRegistryInstance.instance
          .getDAOMemberStorage(daoForm.constitutionVote),
      }
      : null;

    const generalUpdateVeto = daoForm.generalUpdateVote && daoForm.generalUpdateVote !== 'default-no-veto'
      ? {
        target: await daoRegistryInstance.instance.getGeneralDAOVoting(DAO_RESERVED_NAME),
        name: 'Veto General Update Vote',
        initialMembers: [],
        linkedMemberStorage: await daoRegistryInstance.instance
          .getDAOMemberStorage(daoForm.generalUpdateVote),
      }
      : null;

    const vetoGroups = [
      constitutionVeto,
      generalUpdateVeto,
      ...membershipVetoGroups.flat(),
      ...expertPanelVetoGroups.flat()
    ];

    return masterDaoFactoryInstance?.configureVetoGroups(
      daoRegistryAddress,
      vetoGroups.filter(Boolean) as IPermissionManager.VetoGroupInitializationParamsStruct[]);
  };

  const deployQRC20Token = async (txId?: string) => {
    const { daoForm } = getState().dao;
    if (!daoForm?.customErc20Token || !masterContractsRegistryInstance || !currentProvider.currentSigner) return;
    const tokenFactoryAddress = await masterContractsRegistryInstance.instance.getTokenFactory();
    const tokenFactory = getTokenFactoryInstance(tokenFactoryAddress, currentProvider.currentSigner);
    const receipt = await (await tokenFactory.deployQRC20(
      {
        ...daoForm.customErc20Token,
        ...!daoForm.customErc20Token.contractURI && { contractURI: DEFAULT_CONTRACT_URI },
        totalSupplyCap: toBigNumber(daoForm.customErc20Token.totalSupplyCap)
          .multipliedBy(toBigNumber(10).pow(daoForm.customErc20Token.decimals)).toFixed(),
      },
      currentProvider.selectedAddress,
    )).wait();

    const existingTokenAddress = tokenFactory.getTransactionEvents(receipt).DeployedQRC20.args?.token || '';
    if (txId) { updateTransaction(txId, { hash: receipt.transactionHash }); }
    dispatch(setDaoForm({ ...daoForm, ...existingTokenAddress && { tokenAddress: existingTokenAddress } }));
  };

  const deployQRC721Token = async (txId?: string) => {
    const { daoForm } = getState().dao;
    if (!daoForm?.customErc721Token || !masterContractsRegistryInstance || !currentProvider.currentSigner) return;
    const tokenFactoryAddress = await masterContractsRegistryInstance.instance.getTokenFactory();
    const tokenFactory = getTokenFactoryInstance(tokenFactoryAddress, currentProvider.currentSigner);
    const receipt = await (await tokenFactory.deployQRC721(
      {
        ...daoForm.customErc721Token,
        ...!daoForm.customErc721Token.contractURI && { contractURI: DEFAULT_CONTRACT_URI },
      },
      currentProvider.selectedAddress
    )).wait();
    const existingTokenAddress = tokenFactory.getTransactionEvents(receipt).DeployedQRC721.args?.token || '';

    if (txId) { updateTransaction(txId, { hash: receipt.transactionHash }); }
    dispatch(setDaoForm({ ...daoForm, ...existingTokenAddress && { tokenAddress: existingTokenAddress } }));
  };

  return {
    daoForm,
    daoRegistryAddress,

    setDeployForm: useCallback(setDeployForm, []),
    deployDaoParams: useCallback(deployDaoParams, []),
    deployVetoParams: useCallback(deployVetoParams, []),
    deployGuardian: useCallback(deployGuardian, []),
    deployPanel: useCallback(deployPanel, []),
    deployQRC20Token: useCallback(deployQRC20Token, []),
    deployQRC721Token: useCallback(deployQRC721Token, []),
  };
}
