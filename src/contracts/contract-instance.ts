import { DAORegistryInstance, MasterContractsRegistryInstance, MasterDAOFactoryInstance, TokenFactoryInstance } from '@q-dev/gdk-sdk';
import { Signer } from 'ethers';

export const getMasterDaoFactoryInstance = (contractAddress: string, signer: Signer) => {
  return new MasterDAOFactoryInstance(signer, contractAddress);
};

export const getDaoRegistryInstance = (contractAddress: string, signer: Signer) => {
  return new DAORegistryInstance(signer, contractAddress); ;
};

export const getMasterContractsRegistryInstance = (contractAddress: string, signer: Signer) => {
  return new MasterContractsRegistryInstance(signer, contractAddress);
};

export const getTokenFactoryInstance = (contractAddress: string, signer: Signer) => {
  return new TokenFactoryInstance(signer, contractAddress);
};
